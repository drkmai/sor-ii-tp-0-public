#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/errno.h>

int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "maidevice"
#define BUF_LEN 80 /* Max length del mensaje para el device, se puede aumentar.*/
static int Major;
static int Device_Open = 0;
static char msg[BUF_LEN];
static char *msg_Ptr;
static int caesarKey = 20;

static struct file_operations fops = {
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release
};

int init_module(void)
{
    Major = register_chrdev(0, DEVICE_NAME, &fops);
    if (Major < 0)
    {
        printk(KERN_ALERT "El intento de registrar el char device falló con major number %d\n", Major);
        return Major;
    }
    printk(KERN_INFO "UNGS: SOR II, TP 0 MAI - Driver registrado. con major number %d\n",Major);
    printk(KERN_INFO "'Hacer mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
    printk(KERN_INFO "'No olvidar dar permisos con chmod a /dev/%s para poder escribir el archivo con echo.\n",DEVICE_NAME);
    return SUCCESS;
}

void cleanup_module(void)
{
    unregister_chrdev(Major, DEVICE_NAME);
    printk(KERN_INFO "UNGS: SOR II, TP 0 MAI - Driver desregistrado.\n");
    printk(KERN_INFO "No olvide borrar el archivo antes de volver a registrar el device.\n");
}

char caesarCypherEncrypt(char input)
{
  if (input >= 'A' && input <= 'Z')
  {
      input = ((input-'A') + caesarKey) % 26 + 'A';
  }
  else if(input >= 'a' && input <= 'z')
  {
      input = ((input-'a') + caesarKey) % 26 + 'a';
  }

  return input;
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("UNGS-MAI");
MODULE_DESCRIPTION("SOR II TP 0 UNGS");
static int device_open(struct inode *inode, struct file *file)
{
    if (Device_Open)
        return -EBUSY;
    Device_Open++;

    msg_Ptr = msg;
    try_module_get(THIS_MODULE);
    return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file)
{
    Device_Open--;
    module_put(THIS_MODULE);
    return 0;
}

static ssize_t device_read(struct file *filp, char *buffer, size_t length, loff_t * offset)
{
    int bytes_read = 0;
    if (*msg_Ptr == 0)
    {
        return 0;
    }
    while (length && *msg_Ptr)
    {
        put_user(caesarCypherEncrypt(*(msg_Ptr++)), buffer++);
        length--;
        bytes_read++;
    }
    return bytes_read;
}

static ssize_t device_write(struct file *file, const char __user * buffer, size_t length, loff_t * offset)
{
    int i;
    memset(msg, 0, sizeof(msg));
    for (i = 0; i < length && i < BUF_LEN; i++)  
        get_user(msg[i], buffer + i);
    msg_Ptr = msg;
    return i;
}