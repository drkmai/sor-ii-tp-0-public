# Sistemas Operativos y Redes II - TP 0

- **run.sh** es un script para inicializar todo automáticamente. Hace el make, carga el driver, hace el dev file y le da permisos 777. **(Pide prompt de sudo password).**

![](/images/runsh.png)

- **echocat.sh** se puede usar para hacer un echo al dev file rapido y un cat para ver el mensaje que pusimos. **(Pide prompt de sudo password).**

![](/images/echocatsh.png)

- **cleanup.sh** descarga el driver y elimina el archivo.

![](/images/cleanupsh.png)

## Ejecucción run.sh

![](/images/ejecuccionrunsh.png)

## Ejecucción cleanup.sh

![](/images/ejecuccioncleanupsh.png)

## Ejecucción run.sh, con caesar cypher n = 20

![](/images/caesarcypher1.png)

## Ejecucción echochat.sh con caesar cypher n = 20

![](/images/caesarcypher2.png)